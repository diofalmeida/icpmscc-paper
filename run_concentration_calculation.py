import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import CC_method.datatools as dtt

from CC_method.standards_database.read_database import\
	 build_standard_concentrations as get_standards

# --- #

filename = "SurveyData/B2A2W2_MBM_R2A_surveyintensities.xlsx"

stocks_dict = {"MBM": ["REE", "PM27"],\
				"R2A": ["W", "PM26", "PM27"]}

corners_dict = {"MBM": ("C9", "BX33"),\
				"R2A": ("C45", "BX67")}

for medium in stocks_dict:
	writer = pd.ExcelWriter("%s_calibration.xlsx" % medium, engine = 'xlsxwriter')

	ulc, brc = corners_dict[medium]

	samples, element_list, intensities = dtt.import_intensities_info(filename,\
																	 ul_cnr = ulc,\
																	 br_cnr = brc)

	element_syms = dtt.convert_elements_to_sym(element_list)
	std_concentrations = get_standards(stocks_dict[medium], element_syms)
	std_concentrations = (1e3)*std_concentrations #convert c0 to units of ppb

	intensities = (1e-3)*intensities #convert Y to units of kcps

	dfactors = dtt.read_dfactors_from_std_samples(samples, ask_for_confirmation = False)

	# --- # select elements in stocks:

	is_in_stocks = std_concentrations.astype(bool)

	nelms_new = np.sum(is_in_stocks)
	nelms_old = len(element_syms)

	nsamps = intensities.shape[0]

	maincalcon = np.empty(nelms_new, dtype = float)
	element_syms_new = np.empty(nelms_new, dtype = object)

	intensities_new = np.empty((nsamps, nelms_new), dtype = float)
	colnum = 0
	for j in range(nelms_old):
		if is_in_stocks[j]:
			intensities_new[:, colnum] = intensities[:, j]
			element_syms_new[colnum] = element_syms[j]
			maincalcon[colnum] = std_concentrations[j]
			
			colnum += 1

	intensities = np.array(intensities_new)
	element_syms = np.array(element_syms_new)

	# --- # NF matricial calibration:

	ncal = dfactors.size

	Ymat = intensities[:ncal]

	Xmat = np.empty((ncal, 2), dtype = float)
	for k in range(2):
		Xmat[:, k] = dfactors**k

	Hmat = np.matmul(Xmat.T, Xmat)
	Hinv = dtt.myinv(Hmat)

	Bmat = np.matmul(Hinv, Xmat.T)

	Amat = np.identity(ncal) - np.matmul(Xmat, Bmat)

	Smat = dtt.mysandwich(Amat, Ymat)*1.0/(ncal - 2)

	corrmat = np.empty(Smat.shape, dtype = float)
	for i in range(Smat.shape[0]):
		for j in range(Smat.shape[1]):
			corrmat[i, j] = np.sqrt(Smat[i, i]*Smat[j, j])

	corrmat = Smat*1.0/corrmat

	covariance_df = pd.DataFrame(columns = element_syms, index = element_syms, data = Smat)
	covariance_df.to_excel(writer, sheet_name = "NF element x-covariance")

	correlation_df = pd.DataFrame(columns = element_syms, index = element_syms, data = corrmat)
	correlation_df.to_excel(writer, sheet_name = "NF element x-correlation")

	s2vec = np.diag(Smat)

	beta = np.matmul(Bmat, Ymat)

	beta_std = np.empty(beta.shape, dtype = float)
	for k in range(2):
		beta_std[k] = np.sqrt(s2vec*Hinv[k, k])

	# --- # FZ matricial calibration:

	hval = np.matmul(dfactors, dfactors)
	hinv = 1.0/hval

	Bvec = hinv*dfactors.reshape((1, ncal))
	xvec = dfactors.reshape((ncal, 1))

	Amfz = np.identity(ncal) - np.matmul(xvec, Bvec)

	Smfz = dtt.mysandwich(Amfz, Ymat)*1.0/(ncal - 1)

	corrmat = np.empty(Smfz.shape, dtype = float)
	for i in range(Smfz.shape[0]):
		for j in range(Smfz.shape[1]):
			corrmat[i, j] = np.sqrt(Smfz[i, i]*Smfz[j, j])

	corrmat = Smfz*1.0/corrmat

	covariance_df = pd.DataFrame(columns = element_syms, index = element_syms, data = Smfz)
	covariance_df.to_excel(writer, sheet_name = "FZ element x-covariance")

	correlation_df = pd.DataFrame(columns = element_syms, index = element_syms, data = corrmat)
	correlation_df.to_excel(writer, sheet_name = "FZ element x-correlation")

	s2vfz = np.diag(Smfz)

	slope = np.matmul(Bvec, Ymat)

	slope_std = np.sqrt(hinv*s2vfz)

	# --- # FB matricial calibration:

	Yblk = Ymat - Ymat[0]

	Smfb = dtt.mysandwich(Amfz, Yblk)*1.0/(ncal - 2)

	corrmat = np.empty(Smfb.shape, dtype = float)
	for i in range(Smfb.shape[0]):
		for j in range(Smfb.shape[1]):
			corrmat[i, j] = np.sqrt(Smfb[i, i]*Smfb[j, j])

	corrmat = Smfb*1.0/corrmat

	covariance_df = pd.DataFrame(columns = element_syms, index = element_syms, data = Smfb)
	covariance_df.to_excel(writer, sheet_name = "FB element x-covariance")

	correlation_df = pd.DataFrame(columns = element_syms, index = element_syms, data = corrmat)
	correlation_df.to_excel(writer, sheet_name = "FB element x-correlation")

	s2vfb = np.diag(Smfb)

	slope_blk = np.matmul(Bvec, Yblk)
	slope_blk_std = np.sqrt(hinv*s2vfb)

	# --- #

	del covariance_df
	del correlation_df

	# --- #

	caltype_FZ = beta[0] < 0
	caltype_FB = np.logical_not(caltype_FZ) & (beta[0] < beta_std[0])
	caltype_NF = np.logical_not(caltype_FZ | caltype_FB)

	parameters = ["apar", "bpar", "Var00", "Var11", "Cov01", "VarEp", "dof"]

	calibration_df = pd.DataFrame(index = element_syms, columns = parameters)

	for (i, element) in zip(range(nelms_new), element_syms):
		if caltype_NF[i]:
			parlist = np.array([beta[0, i], beta[1, i], s2vec[i]*Hinv[0, 0],\
								s2vec[i]*Hinv[1, 1], s2vec[i]*Hinv[0, 1], s2vec[i],\
								ncal - 2])
			
		elif caltype_FZ[i]:
			parlist = np.array([0.0, slope[0, i], 0.0, hinv*s2vfz[i], 0.0, s2vfz[i],\
								ncal - 1])
		
		elif caltype_FB[i]:
			parlist = np.array([Ymat[0, i], slope_blk[0, i], 0.0, hinv*s2vfb[i], 0.0, s2vfb[i],\
								ncal - 2])
		
		else:
			print(">> ERROR: something wrong with calibration types <<")
		
		for (parname, parval) in zip(parameters, parlist):
			calibration_df.at[element, parname] = parval

	calibration_df.to_excel(writer, sheet_name = "Calibration parameters")

	writer.save()

	# --- #

	writer = pd.ExcelWriter("%s_concentrations.xlsx" % medium, engine = 'xlsxwriter')

	msamples = samples[ncal:]
	msamples_num = msamples.shape[0]

	mintensities = intensities[ncal:]

	mapar = dtt.extend_1D_array(calibration_df["apar"], msamples_num)
	mbpar = dtt.extend_1D_array(calibration_df["bpar"], msamples_num)

	mstds = dtt.extend_1D_array(maincalcon, msamples_num)

	X_est = (mintensities - mapar)*1.0/mbpar
	C_est = mstds*X_est

	concentrations_df = pd.DataFrame(index = msamples[:, 0], columns = element_syms,\
									 data = C_est)

	concentrations_df.to_excel(writer, sheet_name = "Concentrations (ppb)")

	mvar00 = dtt.extend_1D_array(calibration_df["Var00"], msamples_num)
	mvar11 = dtt.extend_1D_array(calibration_df["Var11"], msamples_num)
	mcov01 = dtt.extend_1D_array(calibration_df["Cov01"], msamples_num)
	mvarEp = dtt.extend_1D_array(calibration_df["VarEp"], msamples_num)

	sigC_est = (mstds/mbpar)*np.sqrt(mvarEp + mvar00 + 2*mcov01*X_est + mvar11*(X_est**2))

	sigmacons_df = pd.DataFrame(index = msamples[:, 0], columns = element_syms,\
								data = sigC_est)

	sigmacons_df.to_excel(writer, sheet_name = "Est. std. devs (ppb)")

	# --- #

	surveyints_df = pd.DataFrame(columns = element_syms,\
								 index = msamples[:, 0], data = mintensities)
	
	surveyints_df.to_excel(writer, sheet_name = "Survey intensities (kcps)")

	# --- # concentration covariances:

	for (i, element) in zip(range(nelms_new), element_syms):
		sname = "%s covariances (ppb^2)" % element
		
		var00 = calibration_df.at[element, "Var00"]
		var11 = calibration_df.at[element, "Var11"]
		cov01 = calibration_df.at[element, "Cov01"]
		varEp = calibration_df.at[element, "VarEp"]
		
		C0 = maincalcon[i]
		bpar = calibration_df.at[element, "bpar"]
		
		xvec = X_est[:, i]
		
		xvec, yvec = np.meshgrid(xvec, xvec)
		
		covdata = (varEp*np.identity(msamples_num) + var00 +\
				   cov01*(xvec + yvec) + var11*xvec*yvec)*(C0**2)/(bpar**2)
		
		covar_df = pd.DataFrame(index = msamples[:, 0], columns = msamples[:, 0], data = covdata)
		
		covar_df.to_excel(writer, sheet_name = sname)

	# --- #

	writer.save()
