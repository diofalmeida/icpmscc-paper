"""
This script is run to build a database of the concentrations of each element
in solutions made using the stocks available in the Environmental
Microbiology lab of the University of Coimbra. It takes into account the
stock solutions available as well as the usual procedure followed in this
lab to make external standards for ICP-MS measurements.
(as of December 2021)
"""

import numpy as np
import pandas as pd

# --- #

PM26 = {"Al" : 10.0,\
		"As" : 1.0,\
		"B"  : 10.0,\
		"Cd" : 0.3,\
		"Cr" : 5.0,\
		"Cu" : 10.0,\
		"Fe" : 30.0,\
		"Mn" : 5.0,\
		"Mo" : 7.0,\
		"Ni" : 1.0,\
		"Pb" : 1.0,\
		"Sb" : 0.2,\
		"Se" : 1.0,\
		"U"  : 0.2,\
		"Zn" : 10.0}

PM27_elm = ["Al", "As", "Ba", "Be", "Bi",  "B", "Cd", "Ca", "Cs", "Cr", "Co",\
			"Cu", "Ga", "In", "Fe", "Pb", "Li", "Mg", "Mn", "Ni",  "P",  "K",\
			"Rb", "Se", "Si", "Ag", "Na", "Sr",  "S", "Te", "Tl",  "V", "Zn"]
PM27 = {}
for elm in PM27_elm:
	PM27[elm] = 10.0
	
PM28_elm = ["B", "Si", "P", "S", "Ge", "As", "Se", "Sn", "Sb", "Te"]
PM28 = {}
for elm in PM28_elm:
	PM28[elm] = 10.0

PM167_elm = ["Sb", "Ge", "Au", "Hf", "Ir", "Mo", "Nb", "Pd", "Pt",\
			 "Re", "Rh", "Ru", "Ta", "Sn", "Ti",  "W", "Zr"]
PM167 = {}
for elm in PM167_elm:
	PM167[elm] = 10.0

W = {"W" : 10.0}

REE_elm = ["Sc",  "Y", "La", "Ce", "Pr", "Nd", "Sm", "Eu", "Gd",\
		   "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu"]
REE = {}
for elm in REE_elm:
	REE[elm] = 10.0

# --- #

fdir_list = __file__.split("/")
fdir = ""
if len(fdir_list) > 1:
	for text in fdir_list[:-1]:
		fdir += "/%s" % text
	fdir += "/"

Z_df = pd.read_csv("%selementlist.csv" % fdir, header = None,\
				   names = ["Sym", "Name"], index_col = 0)

Sym_to_Z = {}
for znum in range(1, 119):
	Sym_to_Z[Z_df["Sym"][znum]] = znum

# --- #

Nelm = Sym_to_Z["U"] - Sym_to_Z["Li"] + 1

elements = Z_df["Sym"][(Sym_to_Z["Li"] - 1):Sym_to_Z["U"]].to_numpy(dtype = str)
std_names = {"PM26" : PM26, "PM27" : PM27, "PM28" : PM28, "PM167" : PM167,\
				"W" : W, "REE" : REE}

db_df = pd.DataFrame(data = np.zeros((6, Nelm), dtype = float),\
					 index = std_names.keys(),\
					 columns = elements)

for name in std_names:
	for elm in elements:
		std_type = std_names[name]
		if elm in std_type.keys():
			db_df.at[name, elm] = std_type[elm]

# --- # f factor calculations:

ffacs = 1.0 + (1e-6)*np.sum(db_df.to_numpy(), axis = 1)
new_ffacs = np.empty((6, Nelm), dtype = float)
for i in range(Nelm):
	new_ffacs[:, i] = ffacs

db_df = db_df*1.0/new_ffacs

# --- #

db_df.to_excel("standards_database.xlsx")
