import numpy as np
import pandas as pd

# --- #

fdir_list = __file__.split("/")
fdir = ""
if len(fdir_list) > 1:
	for text in fdir_list[:-1]:
		fdir += "/%s" % text
	fdir += "/"
	
# --- #

db_df = pd.read_excel("%sstandards_database.xlsx" % fdir, index_col = 0)

def build_standard_concentrations (list_of_types, element_syms, database = db_df):
	ntypes = len(list_of_types)
	nelms = len(element_syms)
	std_con = np.zeros(nelms, dtype = float)
	
	if "PM26" in list_of_types:
		factor = 0.2
	else:
		factor = 0.1
			
	for i in range(nelms):
		elm = element_syms[i]
		sval = 0.0
		for std_type in list_of_types:
				sval += database[elm][std_type]
		
		std_con[i] = sval
	
	return std_con*factor
