import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from scipy.stats import t as studt
from tqdm import tqdm

# --- # tools for converting Excel cell notation to row and column numbers in data frames:

letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',\
		   'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

numbers = {}
for i in range(26):
	numbers[letters[i]] = i + 1

def alphaconv (cell, lineskip = 1):
	"""
	Converts Excel cell notation to row and column numbers in data frames.
	
	Args:
		cell (str): Excel cell
		lineskip (int): difference between excel line number and data frame line number(default is 1)
	Returns:
		lnum (int): line number in data frame
		cnum (int): column number in data frame
	
	Example: alphaconv("A2") = (1, 0) if lineskip = 1 and (0, 0) if lineskip = 2
	"""
	
	char = 0
	isletter = True
	while isletter:
		if cell[char] not in letters:
			isletter = False
		else:
			char += 1
	
	collumn = cell[:char]
	line = int(cell[char:])
	 
	cnum = -1
	for i in range(len(collumn)):
		inum = numbers[collumn[-(1 + i)]]
		cnum += inum*(26**(i))
	
	lnum = line - lineskip
	
	return(lnum, cnum)

# --- #

def import_intensities_info (filename, ul_cnr = "C5", br_cnr = "BX44", sheet_name = 0):
	"""
	Tool for importing data required directly from RawSurveyData Excel file.
	
	Args:
		filename: name or full directory of excel file to import - must include file extension
		ul_cnr: upper-left corner excel cell notation of the first intensity value cell - should correspond to measured intensity of 7Li in purified water (default is 'C5')
		br_cnr: bottom_right corner excel cell notation of the last cell containing intensities - should correspond to the measured intensity of 238U in the last sample (default is 'BX44')
		sheet_name: name or number - starting from 0 - of the data sheet to be read from the excel file (default is 0, meaning the first sheet)
	
	Returns:
		sample_dsp: (S, 2)-shaped 2D array containing the sample IDs - first column - and the sample names - 2nd column - of the samples being analyzed, where S is the number of samples, including the standard ones
		elements: 1D array containing the descriptions of every element in the excel file - typically 74 elements. E.g.: '7Li (KED)'
		intensities: (S, E)-shaped 2D array containing the intensity values for all samples for all elements measured
	
	Notes:
		In the excel file, the two columns which contain the information in sample_dsp are assumed to be located 1 and 2 columns to the left of ul_cnr, stating from the row of ul_cnr and ending on the row of br_cnr.
		The row which contains the information on elements is assumed to be 2 rows above ul_cnr, stating from the column of ul_cnr and ending on the column of br_cnr. IMPORTANT: this row should *not* be the first row in the excel file.
	"""
	
	l0, c0 = alphaconv(ul_cnr, lineskip = 2)
	l1, c1 = alphaconv(br_cnr, lineskip = 2)
	
	data_frame = pd.read_excel(filename, sheet_name = sheet_name, index_col = None)
	data_frame = data_frame.to_numpy(dtype = object)
	
	# --- #
	
	intensities = data_frame[l0 : (l1 + 1), c0 : (c1 + 1)].astype(float)
	
	elements = data_frame[l0 - 2, c0 : (c1 + 1)]
	
	sample_dsp = data_frame[l0 : (l1 + 1), (c0 - 2) : c0]
	
	# --- #
	
	return (sample_dsp, elements, intensities)

def convert_elements_to_sym (elements):
	nelms = elements.size
	syms = np.empty(nelms, dtype = object)
	for i in range(nelms):
		element_des = elements[i].split(" ")[0]
	
		c = 0
		while element_des[c] not in letters:
			c += 1
		
		syms[i] = element_des[c:]
	
	return syms

def read_dfactors_from_std_samples (samples, standard_text = "Padrao", H2O_text = "AGUA",\
									ask_for_confirmation = True):
	sample_names = samples[:, 1]
	
	if sample_names[0] != H2O_text:
		raise ValueError("Purified water description not as expected.")
		return None
	
	line = 1
	while (sample_names[line] != standard_text) and (line < sample_names.size):
		line += 1
	
	if line == sample_names.size:
		raise ValueError("Standard sample description not as expected.")
		return None
	
	std_names = sample_names[:line+1]
	
	dfactors = np.zeros(line + 1, dtype = float)
	dfactors[-1] = 1.0
	
	clen = len(standard_text)
	for i in range(1, line):
		dfactors[i] = eval(std_names[i][clen:])
	
	if ask_for_confirmation:
		dfactor_data = np.empty((2, line + 1), dtype = object)
		dfactor_data[0] = std_names
		dfactor_data[1] = dfactors
		dfactor_df = pd.DataFrame(data = dfactor_data.transpose(),\
								  columns = ["sample name", "d-factor"])
		
		print("> Standard samples information and dilution factors:\n")
		print(dfactor_df)
		answer = input("\nDo you confirm these? (Y/n)")
		if "n" in answer or "N" in answer:
			raise ValueError("d-factors reading terminated")
			return None
			
	return dfactors

# --- # added post-script:

def mydet (matrix22):
	return matrix22[0, 0]*matrix22[1, 1] - matrix22[0, 1]*matrix22[1, 0]

def myinv (matrix22):
	return np.array([[matrix22[1, 1], -matrix22[0, 1]],\
					 [-matrix22[1, 0], matrix22[0, 0]]])*1.0/mydet(matrix22)

def mysandwich (matrix, vector):
	tmp = np.matmul(matrix, vector)
	return np.matmul(vector.T, tmp)

def extend_1D_array (array, newdim, dtype = float):
	new_array = np.empty((newdim, array.size), dtype = dtype)
	
	for i in range(newdim):
		new_array[i] = array
	
	return new_array
